/* simplified code for conductivity sensor
more details at http://www.c2o.pro.br/hackaguas/ar01s14.html
*/

byte electrode_1 = 7; //Pin eletrodo
byte electrode_2 = 8; //Pin eletrodo
int  reading_pin = 0; //Analog Pin (reading)
int  reading;         //storing variable
                                                        
void setup() 
{ 
  
  Serial.begin(9600);     
  pinMode(electrode_1, OUTPUT); 
  pinMode(electrode_2, OUTPUT);
}      

void loop()                    
{ 
   
  digitalWrite(electrode_2, HIGH ); //connect eletrodo 2 to positive (5V)
  digitalWrite(electrode_1, LOW);   //Connect eletrodo 1 to negative (0V)
  delay(100);                       //100 milliseconds to stabilize
  reading = analogRead(reading_pin);//Measure tension between voltage divisor
  digitalWrite(electrode_2, LOW);   //Turn off eletrodo 1
  Serial.println(reading);
  delay(500);
} 
