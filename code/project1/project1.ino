#include <DHT.h>
#include <Adafruit_Sensor.h>

#define DHTPIN A1 // connected pin
#define DHTTYPE DHT11 // DHT 11  Defines the type of sensor
/*
If using DHT22, we have to replace DHT11 by DHT22)
*/

//Crreate and object (dht) of type DHT and use the connected pin and sensor type
DHT dht(DHTPIN, DHTTYPE);

void setup()
{
  Serial.begin(9600);
  Serial.println("Testing DHT11");
  dht.begin();
}

void loop() 
{
  // Note that the measurement delay can reach 2 seconds
  float h = dht.readHumidity();
  float t = dht.readTemperature();

// Test if are valid readings, otherwise there is something wrong
  if (isnan(t) || isnan(h)) 
  {
    Serial.println(t);
  } 
  else 
  {
    Serial.print("Humidity: ");
    Serial.print(h);
    Serial.print(" %t");
    Serial.print("Temperature: ");
    Serial.print(t);
    Serial.println(" *C");
  }
}
