#include <OneWire.h>
#include <DallasTemperature.h>
#include <DHT.h>
#include <Adafruit_Sensor.h>

//Air Temperature and Humidity
#define DHTPIN A1 // DHT pin
#define DHTTYPE DHT22 // Type
DHT dht(DHTPIN, DHTTYPE); //define dht using pin and type

//H2O Temperature
#define ONE_WIRE_BUS A2
OneWire oneWire(ONE_WIRE_BUS);

DallasTemperature sensors(&oneWire);
DeviceAddress sensor1;

//Conductivity
byte electrode_1 = 7; //Pin eletrode
byte electrode_2 = 8; //Pin eletrode
int  conductiv_pin = 0; //Analog Pin (reading)
int  reading;         //storing variable

void setup()
{
  Serial.begin(9600);
  pinMode(electrode_1, OUTPUT); 
  pinMode(electrode_2, OUTPUT);

  Serial.println("Testing DHT22");
  Serial.println("Finding sensor DS18B20...");
  Serial.print("Sensors ");
  Serial.print(sensors.getDeviceCount(), DEC);
  Serial.println(" Found.");
  Serial.println();

  dht.begin();
  sensors.begin();
}


void loop() {
  float h = dht.readHumidity(); // delay of DHT22 can reach 2 seconds
  float t = dht.readTemperature();
  
  sensors.requestTemperatures();
  float tempC = sensors.getTempC(sensor1);
 

  digitalWrite(electrode_2, HIGH ); //use eletrode 2 as + (5V)
  digitalWrite(electrode_1, LOW);   //use eletrode 1 as - (GND)
  delay(1000);                         //wait for stabilization
  reading = analogRead(conductiv_pin);  //Measure tension between voltage divisor
  digitalWrite(electrode_2, LOW);     //turn off eletrode 1

    Serial.print("Air humidity(%) ");
    Serial.println(h);
    Serial.print("Air Temp (C) ");
    Serial.println(t);
    Serial.print("Water Temp (C) ");
    Serial.println(sensors.getTempCByIndex(0));
    Serial.print("Conductivity (V) ");
    Serial.println(reading);
    Serial.println();
  delay(500);

}
