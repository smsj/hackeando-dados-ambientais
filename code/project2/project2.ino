/********************************************************************/


//First we include the libraries
#include <OneWire.h> 
#include <DallasTemperature.h>

#define ONE_WIRE_BUS A2 //Data wire is plugged into pin 2 on the Arduino 

OneWire oneWire(ONE_WIRE_BUS); //Setup a oneWire instance to communicate with any OneWire devices  
                               //Not just Maxim/Dallas temperature ICs

DallasTemperature sensors(&oneWire); //Pass our oneWire reference to Dallas Temperature. 


void setup(void) 
{ 
 Serial.begin(9600); //Start serial port
 Serial.println("Dallas Temperature IC Control Library Demo");
 sensors.begin(); //Start up the library 
} 
void loop(void) 
{ 
 //Call sensors.requestTemperatures() to issue a global temperature
 //Request to all devices on the bus
 Serial.print(" Requesting temperatures...");
 sensors.requestTemperatures(); //Send the command to get temperature readings 
 Serial.println("DONE");

 Serial.print("Temperature is: "); 
 Serial.print(sensors.getTempCByIndex(0));
   //More than one DS18B20 can be added on the same bus.
   //0 refers to the first IC on the wire 
   delay(1000); 
} 
