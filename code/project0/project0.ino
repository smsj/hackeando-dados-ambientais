#define PIN_LED 13     // digital pin

int halt = 1000;          //millisec

// execute once when turned on
void setup()
{    
    Serial.begin(9600);
	pinMode(PIN_LED, OUTPUT);
}

// repeated execution
void loop()
{    
	digitalWrite(PIN_LED, HIGH);
	delay(halt);
	digitalWrite(PIN_LED, LOW);
	delay(halt);    
}
